package com.example.timefighter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private var score: Int = 0
    var gameStartedFlag = false
    var initialCountDown: Long = 60000
    var countDownStep: Long = 1000
    private lateinit var countDownTimer: CountDownTimer

    private lateinit var tapMeButton : Button
    private lateinit var gameScoreTextView : TextView
    private lateinit var timeLeftView : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        tapMeButton = findViewById(R.id.tapMeButton)
        gameScoreTextView = findViewById(R.id.scoreView)
        timeLeftView = findViewById(R.id.timeLeftTextView)

        tapMeButton.setOnClickListener { incrementScore() }

        gameScoreTextView.text = getString(R.string.view_score, score)

        resetGame()
    }

    private fun resetGame() {
        score = 0
        gameScoreTextView.text = getString(R.string.view_score, score)

        val initialCountdownTime = initialCountDown / 1000
        timeLeftView.text = getString(R.string.timeLeft, initialCountdownTime)

        countDownTimer = object : CountDownTimer(initialCountDown, countDownStep) {
            override fun onTick(millisUntilFinished: Long) {
                val timeLeft = millisUntilFinished / 1000
                timeLeftView.text = getString(R.string.timeLeft, timeLeft)
            }
            override fun onFinish() {
                endGame()
            }
        }
        gameStartedFlag = false
    }

    private fun incrementScore() {
        if(!gameStartedFlag){
            startGame()
        }
        score++
        val newScore = getString(R.string.view_score, score)
        gameScoreTextView.text = newScore
    }

    private fun startGame(){
        countDownTimer.start()
        gameStartedFlag = true
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.endGameToast, score), Toast.LENGTH_LONG).show()
        resetGame()
    }
} 